pipeline {
    agent { label 'jenkins_slave' }
    stages {
        stage('Clone') {
            steps {
                echo 'Clone project to Jenkin Slave WS'
                git 'https://gitlab.com/Sankhadeep_Das/CI-CD-Docker-Kubernetes-ReporterPortal.git'
            }
        }
        stage('Build') {
            steps {
                echo 'Clean and compile'
                bat label: '', script: 'mvn clean compile'
            }
        }
    	stage('Test') {
            steps {
                echo 'Run Tests'
                bat label: '', script: 'mvn test'
            }
        }
	stage('Test') {
            steps {
                echo 'Publish Results'
            }
        }
    } 
}
