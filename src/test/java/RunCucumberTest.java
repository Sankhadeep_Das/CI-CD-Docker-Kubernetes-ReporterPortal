import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {"pretty", "com.epam.reportportal.cucumber.ScenarioReporter",
        "json:target/Cucumber.json",
        "html:target/cucumber-html-report"},
        features = "src/test/resources/features")
public class RunCucumberTest {}
